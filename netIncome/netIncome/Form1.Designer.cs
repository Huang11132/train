﻿namespace netIncome
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.LblIncome = new System.Windows.Forms.Label();
            this.LblGrade = new System.Windows.Forms.Label();
            this.LblTaxRate = new System.Windows.Forms.Label();
            this.LblDiscount = new System.Windows.Forms.Label();
            this.LblTax = new System.Windows.Forms.Label();
            this.LblMsg = new System.Windows.Forms.Label();
            this.TxtNetIncome = new System.Windows.Forms.TextBox();
            this.BtnOK = new System.Windows.Forms.Button();
            this.BtnEnd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 16F);
            this.label1.Location = new System.Drawing.Point(186, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(321, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "****所得稅試算表****";
            // 
            // LblIncome
            // 
            this.LblIncome.AutoSize = true;
            this.LblIncome.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblIncome.Location = new System.Drawing.Point(55, 90);
            this.LblIncome.Name = "LblIncome";
            this.LblIncome.Size = new System.Drawing.Size(201, 30);
            this.LblIncome.TabIndex = 1;
            this.LblIncome.Text = "1. 全年所得淨額 : ";
            // 
            // LblGrade
            // 
            this.LblGrade.AutoSize = true;
            this.LblGrade.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblGrade.Location = new System.Drawing.Point(55, 140);
            this.LblGrade.Name = "LblGrade";
            this.LblGrade.Size = new System.Drawing.Size(153, 30);
            this.LblGrade.TabIndex = 2;
            this.LblGrade.Text = "2. 級        距 : ";
            // 
            // LblTaxRate
            // 
            this.LblTaxRate.AutoSize = true;
            this.LblTaxRate.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblTaxRate.Location = new System.Drawing.Point(55, 190);
            this.LblTaxRate.Name = "LblTaxRate";
            this.LblTaxRate.Size = new System.Drawing.Size(153, 30);
            this.LblTaxRate.TabIndex = 3;
            this.LblTaxRate.Text = "3. 所得稅率 : ";
            // 
            // LblDiscount
            // 
            this.LblDiscount.AutoSize = true;
            this.LblDiscount.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblDiscount.Location = new System.Drawing.Point(55, 240);
            this.LblDiscount.Name = "LblDiscount";
            this.LblDiscount.Size = new System.Drawing.Size(153, 30);
            this.LblDiscount.TabIndex = 4;
            this.LblDiscount.Text = "4. 累進差額 : ";
            // 
            // LblTax
            // 
            this.LblTax.AutoSize = true;
            this.LblTax.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblTax.Location = new System.Drawing.Point(55, 290);
            this.LblTax.Name = "LblTax";
            this.LblTax.Size = new System.Drawing.Size(153, 30);
            this.LblTax.TabIndex = 5;
            this.LblTax.Text = "5. 應繳稅額 : ";
            // 
            // LblMsg
            // 
            this.LblMsg.AutoSize = true;
            this.LblMsg.Location = new System.Drawing.Point(55, 340);
            this.LblMsg.Name = "LblMsg";
            this.LblMsg.Size = new System.Drawing.Size(82, 23);
            this.LblMsg.TabIndex = 6;
            this.LblMsg.Text = "提示訊息";
            // 
            // TxtNetIncome
            // 
            this.TxtNetIncome.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtNetIncome.Location = new System.Drawing.Point(300, 90);
            this.TxtNetIncome.Name = "TxtNetIncome";
            this.TxtNetIncome.Size = new System.Drawing.Size(185, 39);
            this.TxtNetIncome.TabIndex = 7;
            this.TxtNetIncome.Text = "請輸入 ...";
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.BtnOK.Location = new System.Drawing.Point(523, 90);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(100, 50);
            this.BtnOK.TabIndex = 8;
            this.BtnOK.Text = "試算";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // BtnEnd
            // 
            this.BtnEnd.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.BtnEnd.Location = new System.Drawing.Point(523, 182);
            this.BtnEnd.Name = "BtnEnd";
            this.BtnEnd.Size = new System.Drawing.Size(100, 50);
            this.BtnEnd.TabIndex = 9;
            this.BtnEnd.Text = "結束";
            this.BtnEnd.UseVisualStyleBackColor = true;
            this.BtnEnd.Click += new System.EventHandler(this.BtnEnd_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(678, 439);
            this.Controls.Add(this.BtnEnd);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.TxtNetIncome);
            this.Controls.Add(this.LblMsg);
            this.Controls.Add(this.LblTax);
            this.Controls.Add(this.LblDiscount);
            this.Controls.Add(this.LblTaxRate);
            this.Controls.Add(this.LblGrade);
            this.Controls.Add(this.LblIncome);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "所得稅試算表";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblIncome;
        private System.Windows.Forms.Label LblGrade;
        private System.Windows.Forms.Label LblTaxRate;
        private System.Windows.Forms.Label LblDiscount;
        private System.Windows.Forms.Label LblTax;
        private System.Windows.Forms.Label LblMsg;
        private System.Windows.Forms.TextBox TxtNetIncome;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Button BtnEnd;
    }
}

