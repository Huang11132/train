﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace netIncome
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LblIncome.Text = "1. 全年淨所得 : ";
            LblGrade.Text = "2. 級      距 : ";
            LblTaxRate.Text = "3. 所得稅率 : ";
            LblDiscount.Text = "4. 累進差額 : ";
            LblTax.Text = "5. 應繳稅額 : ";
            TxtNetIncome.Text = "請輸入 ...";
            LblMsg.Text = "提示訊息";
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            int grade = 0;
            double tax = 0, taxRate = 0, discount = 0;
            double netIncome = double.Parse(TxtNetIncome.Text);
            if (netIncome > 0)
            {
                grade = netIncome <= 520000 ? 1 : (netIncome <= 1170000 ? 2 : (netIncome <=
                    2350000 ? 3 : (netIncome <= 4400000 ? 4 : (netIncome <= 10000000 ? 5 : 6))));
                switch (grade)
                {
                    case 1:
                        taxRate = 5;
                        discount = 0;
                        tax = netIncome * taxRate / 100;
                        break;
                    case 2:
                        taxRate = 12;
                        discount = 36400;
                        tax = netIncome * taxRate / 100 - discount;
                        break;
                    case 3:
                        taxRate = 20;
                        discount = 130000;
                        tax = netIncome * taxRate / 100 - discount;
                        break;
                    case 4:
                        taxRate = 30;
                        discount = 365000;
                        tax = netIncome * taxRate / 100 - discount;
                        break;
                    case 5:
                        taxRate = 40;
                        discount = 805000;
                        tax = netIncome * taxRate / 100 - discount;
                        break;
                    case 6:
                        taxRate = 45;
                        discount = 1305000;
                        tax = netIncome * taxRate / 100 - discount;
                        break;
                    default:
                        LblMsg.Text = " **** 無此級距 **** ";
                        break;
                }
                LblGrade.Text = "2. 級      距 : " + grade.ToString();
                LblTaxRate.Text = "3. 所得稅率 : " + taxRate.ToString() + "%";
                LblDiscount.Text = "4. 累進差額 : " + discount.ToString() + "元";
                LblTax.Text = "5. 應繳稅額 : " + tax.ToString() + "元";
                LblMsg.Text = "應繳稅額 = 綜合所得淨額 × 稅率 - 累進差額";
            }
            else
            {
                LblMsg.Text = "**** 不用扣稅 ****";
                LblGrade.Text = "2. 級      距 : 1";
                LblTaxRate.Text = "3. 所得稅率 : 0 %";
                LblDiscount.Text = "4. 累進差額 : 0 元";
                LblTax.Text = "5. 應繳稅額 : 0 元";
            }            
        }

        private void BtnEnd_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
