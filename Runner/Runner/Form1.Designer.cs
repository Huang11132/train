﻿namespace Runner
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ImgRun = new System.Windows.Forms.ImageList(this.components);
            this.TmrRun = new System.Windows.Forms.Timer(this.components);
            this.TmrMove = new System.Windows.Forms.Timer(this.components);
            this.PicStop = new System.Windows.Forms.PictureBox();
            this.PicStart = new System.Windows.Forms.PictureBox();
            this.PicRun = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRun)).BeginInit();
            this.SuspendLayout();
            // 
            // ImgRun
            // 
            this.ImgRun.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgRun.ImageStream")));
            this.ImgRun.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgRun.Images.SetKeyName(0, "man1.gif");
            this.ImgRun.Images.SetKeyName(1, "man2.gif");
            this.ImgRun.Images.SetKeyName(2, "man3.gif");
            this.ImgRun.Images.SetKeyName(3, "man4.gif");
            // 
            // TmrRun
            // 
            this.TmrRun.Tick += new System.EventHandler(this.TmrRun_Tick);
            // 
            // TmrMove
            // 
            this.TmrMove.Tick += new System.EventHandler(this.TmrMove_Tick);
            // 
            // PicStop
            // 
            this.PicStop.Image = global::Runner.Properties.Resources.stop;
            this.PicStop.Location = new System.Drawing.Point(220, 82);
            this.PicStop.Name = "PicStop";
            this.PicStop.Size = new System.Drawing.Size(45, 45);
            this.PicStop.TabIndex = 2;
            this.PicStop.TabStop = false;
            this.PicStop.Click += new System.EventHandler(this.PicStop_Click);
            // 
            // PicStart
            // 
            this.PicStart.Image = global::Runner.Properties.Resources.start;
            this.PicStart.Location = new System.Drawing.Point(153, 82);
            this.PicStart.Name = "PicStart";
            this.PicStart.Size = new System.Drawing.Size(45, 45);
            this.PicStart.TabIndex = 1;
            this.PicStart.TabStop = false;
            this.PicStart.Click += new System.EventHandler(this.PicStart_Click);
            // 
            // PicRun
            // 
            this.PicRun.Location = new System.Drawing.Point(220, 30);
            this.PicRun.Name = "PicRun";
            this.PicRun.Size = new System.Drawing.Size(45, 45);
            this.PicRun.TabIndex = 0;
            this.PicRun.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(278, 144);
            this.Controls.Add(this.PicStop);
            this.Controls.Add(this.PicStart);
            this.Controls.Add(this.PicRun);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRun)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImgRun;
        private System.Windows.Forms.Timer TmrRun;
        private System.Windows.Forms.Timer TmrMove;
        private System.Windows.Forms.PictureBox PicRun;
        private System.Windows.Forms.PictureBox PicStart;
        private System.Windows.Forms.PictureBox PicStop;
    }
}

