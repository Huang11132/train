﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loan
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TextAns.ReadOnly = true;
            TextInputLoan.TabIndex = 0;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string[] lines = { "請輸入數值", "小不點是壓不"};
            Random rnd1 = new Random();
            int number = rnd1.Next(2);
            try
            {
                double loan = Convert.ToDouble(TextInputLoan.Text);
                double rate = Convert.ToDouble(TextInputRate.Text) / 100;
                double year = Convert.ToDouble(TextInputYear.Text);

                double payRate = ((Math.Pow((1 + rate / 12), year * 12) * rate / 12)) /
                    (Math.Pow((1 + rate / 12), year * 12) - 1);

                TextAns.Text = ((int)(loan * payRate + 0.5)).ToString();
            }
            catch
            {
                TextAns.Text = lines[number];
                TextInputLoan.Clear();
                TextInputRate.Clear();
                TextInputYear.Clear();
            }
            TextInputLoan.Focus();
        }
    }
}
