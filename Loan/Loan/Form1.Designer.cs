﻿namespace Loan
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LblLoan = new System.Windows.Forms.Label();
            this.TextInputLoan = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.LblPay = new System.Windows.Forms.Label();
            this.TextAns = new System.Windows.Forms.TextBox();
            this.LblRate = new System.Windows.Forms.Label();
            this.LblYear = new System.Windows.Forms.Label();
            this.TextInputRate = new System.Windows.Forms.TextBox();
            this.TextInputYear = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 14F);
            this.label1.Location = new System.Drawing.Point(69, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(376, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "貸款本息定額每月攤還試算表";
            // 
            // LblLoan
            // 
            this.LblLoan.AutoSize = true;
            this.LblLoan.Font = new System.Drawing.Font("新細明體", 11F);
            this.LblLoan.Location = new System.Drawing.Point(83, 137);
            this.LblLoan.Name = "LblLoan";
            this.LblLoan.Size = new System.Drawing.Size(137, 22);
            this.LblLoan.TabIndex = 1;
            this.LblLoan.Text = "1. 貸款金額 : ";
            // 
            // TextInputLoan
            // 
            this.TextInputLoan.Font = new System.Drawing.Font("新細明體", 11F);
            this.TextInputLoan.Location = new System.Drawing.Point(272, 134);
            this.TextInputLoan.Name = "TextInputLoan";
            this.TextInputLoan.Size = new System.Drawing.Size(150, 34);
            this.TextInputLoan.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 3;
            this.button1.Text = "計算";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // LblPay
            // 
            this.LblPay.AutoSize = true;
            this.LblPay.Font = new System.Drawing.Font("新細明體", 11F);
            this.LblPay.Location = new System.Drawing.Point(83, 370);
            this.LblPay.Name = "LblPay";
            this.LblPay.Size = new System.Drawing.Size(142, 22);
            this.LblPay.TabIndex = 4;
            this.LblPay.Text = "每月應還本息";
            // 
            // TextAns
            // 
            this.TextAns.Font = new System.Drawing.Font("新細明體", 11F);
            this.TextAns.Location = new System.Drawing.Point(272, 367);
            this.TextAns.Name = "TextAns";
            this.TextAns.Size = new System.Drawing.Size(150, 34);
            this.TextAns.TabIndex = 5;
            // 
            // LblRate
            // 
            this.LblRate.AutoSize = true;
            this.LblRate.Font = new System.Drawing.Font("新細明體", 11F);
            this.LblRate.Location = new System.Drawing.Point(83, 192);
            this.LblRate.Name = "LblRate";
            this.LblRate.Size = new System.Drawing.Size(146, 22);
            this.LblRate.TabIndex = 6;
            this.LblRate.Text = "2. 年利率(%) : ";
            // 
            // LblYear
            // 
            this.LblYear.AutoSize = true;
            this.LblYear.Font = new System.Drawing.Font("新細明體", 11F);
            this.LblYear.Location = new System.Drawing.Point(83, 244);
            this.LblYear.Name = "LblYear";
            this.LblYear.Size = new System.Drawing.Size(93, 22);
            this.LblYear.TabIndex = 7;
            this.LblYear.Text = "3. 年數 : ";
            // 
            // TextInputRate
            // 
            this.TextInputRate.Font = new System.Drawing.Font("新細明體", 11F);
            this.TextInputRate.Location = new System.Drawing.Point(272, 189);
            this.TextInputRate.Name = "TextInputRate";
            this.TextInputRate.Size = new System.Drawing.Size(150, 34);
            this.TextInputRate.TabIndex = 8;
            // 
            // TextInputYear
            // 
            this.TextInputYear.Font = new System.Drawing.Font("新細明體", 11F);
            this.TextInputYear.Location = new System.Drawing.Point(272, 244);
            this.TextInputYear.Name = "TextInputYear";
            this.TextInputYear.Size = new System.Drawing.Size(150, 34);
            this.TextInputYear.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 444);
            this.Controls.Add(this.TextInputYear);
            this.Controls.Add(this.TextInputRate);
            this.Controls.Add(this.LblYear);
            this.Controls.Add(this.LblRate);
            this.Controls.Add(this.TextAns);
            this.Controls.Add(this.LblPay);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextInputLoan);
            this.Controls.Add(this.LblLoan);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblLoan;
        private System.Windows.Forms.TextBox TextInputLoan;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label LblPay;
        private System.Windows.Forms.TextBox TextAns;
        private System.Windows.Forms.Label LblRate;
        private System.Windows.Forms.Label LblYear;
        private System.Windows.Forms.TextBox TextInputRate;
        private System.Windows.Forms.TextBox TextInputYear;
    }
}

