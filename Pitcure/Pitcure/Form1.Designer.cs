﻿namespace Pitcure
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.PicDemo1 = new System.Windows.Forms.PictureBox();
            this.PicDemo2 = new System.Windows.Forms.PictureBox();
            this.PicDemo3 = new System.Windows.Forms.PictureBox();
            this.PicDemo4 = new System.Windows.Forms.PictureBox();
            this.PicShow = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicShow)).BeginInit();
            this.SuspendLayout();
            // 
            // PicDemo1
            // 
            this.PicDemo1.Image = ((System.Drawing.Image)(resources.GetObject("PicDemo1.Image")));
            this.PicDemo1.Location = new System.Drawing.Point(40, 40);
            this.PicDemo1.Name = "PicDemo1";
            this.PicDemo1.Size = new System.Drawing.Size(39, 26);
            this.PicDemo1.TabIndex = 0;
            this.PicDemo1.TabStop = false;
            this.PicDemo1.Click += new System.EventHandler(this.PicDemo1_Click);
            // 
            // PicDemo2
            // 
            this.PicDemo2.Image = ((System.Drawing.Image)(resources.GetObject("PicDemo2.Image")));
            this.PicDemo2.Location = new System.Drawing.Point(115, 40);
            this.PicDemo2.Name = "PicDemo2";
            this.PicDemo2.Size = new System.Drawing.Size(39, 26);
            this.PicDemo2.TabIndex = 1;
            this.PicDemo2.TabStop = false;
            this.PicDemo2.Click += new System.EventHandler(this.PicDemo2_Click);
            // 
            // PicDemo3
            // 
            this.PicDemo3.Image = ((System.Drawing.Image)(resources.GetObject("PicDemo3.Image")));
            this.PicDemo3.Location = new System.Drawing.Point(182, 40);
            this.PicDemo3.Name = "PicDemo3";
            this.PicDemo3.Size = new System.Drawing.Size(39, 26);
            this.PicDemo3.TabIndex = 2;
            this.PicDemo3.TabStop = false;
            this.PicDemo3.Click += new System.EventHandler(this.PicDemo3_Click);
            // 
            // PicDemo4
            // 
            this.PicDemo4.Image = ((System.Drawing.Image)(resources.GetObject("PicDemo4.Image")));
            this.PicDemo4.Location = new System.Drawing.Point(251, 40);
            this.PicDemo4.Name = "PicDemo4";
            this.PicDemo4.Size = new System.Drawing.Size(39, 26);
            this.PicDemo4.TabIndex = 3;
            this.PicDemo4.TabStop = false;
            this.PicDemo4.Click += new System.EventHandler(this.PicDemo4_Click);
            // 
            // PicShow
            // 
            this.PicShow.Location = new System.Drawing.Point(40, 92);
            this.PicShow.Name = "PicShow";
            this.PicShow.Size = new System.Drawing.Size(250, 200);
            this.PicShow.TabIndex = 4;
            this.PicShow.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 496);
            this.Controls.Add(this.PicShow);
            this.Controls.Add(this.PicDemo4);
            this.Controls.Add(this.PicDemo3);
            this.Controls.Add(this.PicDemo2);
            this.Controls.Add(this.PicDemo1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDemo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicShow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PicDemo1;
        private System.Windows.Forms.PictureBox PicDemo2;
        private System.Windows.Forms.PictureBox PicDemo3;
        private System.Windows.Forms.PictureBox PicDemo4;
        private System.Windows.Forms.PictureBox PicShow;
    }
}

