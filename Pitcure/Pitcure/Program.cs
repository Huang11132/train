﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pitcure
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form form = new Form();
            UserControl1 c = new UserControl1
            {
                Dock = DockStyle.Fill
            };
            form.Controls.Add(c);
            Application.Run(form);
        }
    }
}
