﻿namespace GPAAvg
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCode = new System.Windows.Forms.TextBox();
            this.TxtScore = new System.Windows.Forms.TextBox();
            this.TxtHour = new System.Windows.Forms.TextBox();
            this.BtnOK = new System.Windows.Forms.Button();
            this.BtnQuit = new System.Windows.Forms.Button();
            this.LblSumGPA = new System.Windows.Forms.Label();
            this.LblSumCourses = new System.Windows.Forms.Label();
            this.LblAvgGPA = new System.Windows.Forms.Label();
            this.TxtMsg = new System.Windows.Forms.TextBox();
            this.TxtTitle = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(60, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "1. 課程代號";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(60, 100);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "2. 學期成績";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.label3.Location = new System.Drawing.Point(60, 160);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 30);
            this.label3.TabIndex = 2;
            this.label3.Text = "3. 學分數";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.label4.Location = new System.Drawing.Point(60, 220);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 30);
            this.label4.TabIndex = 3;
            this.label4.Text = "4. 累計GPA*學分";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.label5.Location = new System.Drawing.Point(60, 280);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(231, 30);
            this.label5.TabIndex = 4;
            this.label5.Text = "5. 目前累計總學分數";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.label6.Location = new System.Drawing.Point(60, 340);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(208, 30);
            this.label6.TabIndex = 5;
            this.label6.Text = "6. 本學期平均GPA";
            // 
            // TxtCode
            // 
            this.TxtCode.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtCode.Location = new System.Drawing.Point(250, 40);
            this.TxtCode.Name = "TxtCode";
            this.TxtCode.Size = new System.Drawing.Size(100, 39);
            this.TxtCode.TabIndex = 7;
            // 
            // TxtScore
            // 
            this.TxtScore.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtScore.Location = new System.Drawing.Point(250, 100);
            this.TxtScore.Name = "TxtScore";
            this.TxtScore.Size = new System.Drawing.Size(100, 39);
            this.TxtScore.TabIndex = 8;
            // 
            // TxtHour
            // 
            this.TxtHour.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtHour.Location = new System.Drawing.Point(250, 160);
            this.TxtHour.Name = "TxtHour";
            this.TxtHour.Size = new System.Drawing.Size(100, 39);
            this.TxtHour.TabIndex = 9;
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.BtnOK.Location = new System.Drawing.Point(400, 40);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(100, 60);
            this.BtnOK.TabIndex = 10;
            this.BtnOK.Text = "累計";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // BtnQuit
            // 
            this.BtnQuit.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.BtnQuit.Location = new System.Drawing.Point(400, 139);
            this.BtnQuit.Name = "BtnQuit";
            this.BtnQuit.Size = new System.Drawing.Size(100, 60);
            this.BtnQuit.TabIndex = 11;
            this.BtnQuit.Text = "重製";
            this.BtnQuit.UseVisualStyleBackColor = true;
            this.BtnQuit.Click += new System.EventHandler(this.BtnQuit_Click);
            // 
            // LblSumGPA
            // 
            this.LblSumGPA.AutoSize = true;
            this.LblSumGPA.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblSumGPA.Location = new System.Drawing.Point(400, 220);
            this.LblSumGPA.Name = "LblSumGPA";
            this.LblSumGPA.Size = new System.Drawing.Size(43, 30);
            this.LblSumGPA.TabIndex = 12;
            this.LblSumGPA.Text = "     ";
            // 
            // LblSumCourses
            // 
            this.LblSumCourses.AutoSize = true;
            this.LblSumCourses.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblSumCourses.Location = new System.Drawing.Point(400, 280);
            this.LblSumCourses.Name = "LblSumCourses";
            this.LblSumCourses.Size = new System.Drawing.Size(43, 30);
            this.LblSumCourses.TabIndex = 13;
            this.LblSumCourses.Text = "     ";
            // 
            // LblAvgGPA
            // 
            this.LblAvgGPA.AutoSize = true;
            this.LblAvgGPA.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.LblAvgGPA.Location = new System.Drawing.Point(400, 340);
            this.LblAvgGPA.Name = "LblAvgGPA";
            this.LblAvgGPA.Size = new System.Drawing.Size(43, 30);
            this.LblAvgGPA.TabIndex = 14;
            this.LblAvgGPA.Text = "     ";
            // 
            // TxtMsg
            // 
            this.TxtMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtMsg.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtMsg.Location = new System.Drawing.Point(60, 437);
            this.TxtMsg.Multiline = true;
            this.TxtMsg.Name = "TxtMsg";
            this.TxtMsg.ReadOnly = true;
            this.TxtMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtMsg.Size = new System.Drawing.Size(440, 92);
            this.TxtMsg.TabIndex = 15;
            // 
            // TxtTitle
            // 
            this.TxtTitle.BackColor = System.Drawing.SystemColors.Control;
            this.TxtTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtTitle.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtTitle.Location = new System.Drawing.Point(60, 399);
            this.TxtTitle.Name = "TxtTitle";
            this.TxtTitle.ReadOnly = true;
            this.TxtTitle.Size = new System.Drawing.Size(440, 32);
            this.TxtTitle.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(573, 574);
            this.Controls.Add(this.TxtTitle);
            this.Controls.Add(this.TxtMsg);
            this.Controls.Add(this.LblAvgGPA);
            this.Controls.Add(this.LblSumCourses);
            this.Controls.Add(this.LblSumGPA);
            this.Controls.Add(this.BtnQuit);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.TxtHour);
            this.Controls.Add(this.TxtScore);
            this.Controls.Add(this.TxtCode);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "學期成績GPA試算";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtCode;
        private System.Windows.Forms.TextBox TxtScore;
        private System.Windows.Forms.TextBox TxtHour;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Button BtnQuit;
        private System.Windows.Forms.Label LblSumGPA;
        private System.Windows.Forms.Label LblSumCourses;
        private System.Windows.Forms.Label LblAvgGPA;
        private System.Windows.Forms.TextBox TxtMsg;
        private System.Windows.Forms.TextBox TxtTitle;
    }
}

