﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPAAvg
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int GPA, score;
        int courses = 0;
        double sumGPA = 0;
        string grade, msg;
        
        private void Form1_Load(object sender, EventArgs e)
        {
            TxtCode.MaxLength = 4;
            TxtHour.MaxLength = 1;
            TxtTitle.Text = "  代號\t成績\t學分數\tGrade\tGPA  ";
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            score = Convert.ToInt32(TxtScore.Text);
            switch (score / 10)
            {
                case 10:
                case 9:
                case 8:
                    GPA = 4;
                    grade = "A";
                    break;
                case 7:
                    GPA = 3;
                    grade = "B";
                    break;
                case 6:
                    GPA = 2;
                    grade = "C";
                    break;
                case 5:
                    GPA = 1;
                    grade = "D";
                    break;
                default:
                    GPA = 0;
                    grade = "E";
                    break;
            }
            courses += Convert.ToInt32(TxtHour.Text);
            LblSumCourses.Text = courses.ToString();
            sumGPA += GPA * Convert.ToInt32(TxtHour.Text);
            LblSumGPA.Text = sumGPA.ToString();
            LblAvgGPA.Text = (sumGPA / courses).ToString("F2");
            msg += "  " + TxtCode.Text + "\t" + score.ToString() +
                "\t" + TxtHour.Text + "\t" + grade + "\t" +
                GPA.ToString() + "\r\n";
            TxtMsg.Text = msg;
        }

        private void BtnQuit_Click(object sender, EventArgs e)
        {
            courses = 0;
            sumGPA = 0;
            msg = "";
            LblSumCourses.Text = courses.ToString();
            LblSumGPA.Text = sumGPA.ToString();
            LblAvgGPA.Text = "";
            TxtMsg.Text = msg;
        }
    }
}
