﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Area
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string uName = Microsoft.VisualBasic.Interaction.InputBox("請輸入姓名","輸入");
            DialogResult dr = MessageBox.Show(uName + "歡迎您!", "歡迎",
                MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            Text = uName;

            TxtW.TabIndex = 0;
            TxtArea.ReadOnly = true;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            try
            {
                double w = Convert.ToDouble(TxtW.Text);
                double h = Convert.ToDouble(TxtH.Text);
                TxtArea.Text = Convert.ToString(w * h * 0.3025) + " 坪";
            }
            catch
            {
                MessageBox.Show("請輸入寬度和長度!","注意",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }
    }
}
